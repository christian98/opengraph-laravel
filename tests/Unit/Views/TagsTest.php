<?php

namespace Unit\Views;

use Illuminate\Support\Facades\Blade;
use OpengraphLaravel\OpengraphLaravel\Opengraph;
use OpengraphLaravel\OpengraphLaravel\Tests\TestCase;

class TagsTest extends TestCase
{
    public function test_should_render_tags(): void
    {
        Opengraph::title('Test Title')
            ->typeWebsite()
            ->url('https://example.com')
            ->image('https://example.com');

        $html = Blade::render(/** @lang Blade */ "<x-opengraph::tags />");
        $this->assertContainsMetaTag($html, 'og:title', 'Test Title');
        $this->assertContainsMetaTag($html, 'og:type', 'website');
        $this->assertContainsMetaTag($html, 'og:url', 'https://example.com');
        $this->assertContainsMetaTag($html, 'og:image', 'https://example.com');
    }
}
