<?php

namespace Unit;

use Illuminate\Support\Carbon;
use OpengraphLaravel\OpengraphLaravel\OpenGraphMetaTag;
use OpengraphLaravel\OpengraphLaravel\Tests\TestCase;
use OpengraphLaravel\OpengraphLaravel\Tests\TestEnum;

class OpengraphMetaTagTest extends TestCase
{
    public function test_should_generate_string_value(): void
    {
        $tag = new OpenGraphMetaTag('test', 'myValue');
        $this->assertMatchesHtmlSnapshot($tag->toHtml());
    }

    public function test_should_generate_number_value(): void
    {
        $tag = new OpenGraphMetaTag('test', 189);
        $this->assertMatchesHtmlSnapshot($tag->toHtml());
    }

    public function test_should_generate_boolean_value(): void
    {
        $tag = new OpenGraphMetaTag('test', true);
        $this->assertMatchesHtmlSnapshot($tag->toHtml());
    }

    public function test_should_generate_float_value(): void
    {
        $tag = new OpenGraphMetaTag('test', 12.45);
        $this->assertMatchesHtmlSnapshot($tag->toHtml());
    }

    public function test_should_generate_date_value(): void
    {
        $tag = new OpenGraphMetaTag('test', Carbon::createFromDate(2022, 9, 10)->setTime(0, 0, 0));
        $this->assertMatchesHtmlSnapshot($tag->toHtml());
    }

    public function test_should_generate_backed_enum_value(): void
    {
        $tag = new OpenGraphMetaTag('test', TestEnum::One);
        $this->assertMatchesHtmlSnapshot($tag->toHtml());
    }

    public function test_should_prevent_injection_value(): void
    {
        $tag = new OpenGraphMetaTag('<script>alert("alarm");</script>', '<script>alert("alarm");</script>');
        $this->assertMatchesHtmlSnapshot($tag->toHtml());
    }
}
