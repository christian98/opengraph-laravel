<?php

namespace OpengraphLaravel\OpengraphLaravel\Tests;

use Carbon\Carbon;
use OpengraphLaravel\OpengraphLaravel\Basic\Image;
use OpengraphLaravel\OpengraphLaravel\ObjectType\Article;
use OpengraphLaravel\OpengraphLaravel\OpenGraphData;

class BasicTest extends TestCase
{
    public function test_should_render_basic_information(): void
    {
        $openGraph = new OpenGraphData();

        $openGraph
            ->title('My title')
            ->image('https://example.com/myFile.jpg', function (Image $image) {
                $image->mimeType('image/jpeg');
            })
            ->typeWebsite()
            ->url('https://example.com');

        $this->assertMatchesHtmlSnapshot($openGraph->toHtml());
    }

    public function test_should_render_a_bit_more_complex_information(): void
    {
        $openGraph = new OpenGraphData();

        $openGraph
            ->title('My title')
            ->image('https://example.com/myFile.jpg', function (Image $image) {
                $image->mimeType('image/jpeg');
            })
            ->url('https://example.com')
            ->typeArticle(function (Article $article) {
                $article->publishedTime(Carbon::create(2022, 7, 8, 16, 19, 2))
                    ->tag('test-tag')
                    ->author('https://christian-hollaender.de/me');
            });

        $this->assertMatchesHtmlSnapshot($openGraph->toHtml());
    }
}
