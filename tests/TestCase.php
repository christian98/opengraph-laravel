<?php

namespace OpengraphLaravel\OpengraphLaravel\Tests;

use Illuminate\Foundation\Application;
use Illuminate\Testing\TestResponse;
use OpengraphLaravel\OpengraphLaravel\OpengraphLaravelServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;
use Spatie\Snapshots\MatchesSnapshots;

/**
 * Override the standard PHPUnit testcase with the Testbench testcase
 *
 * @see https://github.com/orchestral/testbench#usage
 */
abstract class TestCase extends OrchestraTestCase
{
    use MatchesSnapshots;

    /*
        protected function setUp(): void
        {
            parent::setUp();
            TestResponse::macro('assertContainingMetaTag', function (string $name, string $content) {
                return $this->toContain("<meta property=\"$name\" content=\"$content\" />");
            });
        }
    */

    protected function assertContainsMetaTag(string $haystack, string $name, string $content): void
    {
        $this->assertStringContainsString("<meta property=\"$name\" content=\"$content\" />", $haystack);
    }

    /**
     * Include the package's service provider(s)
     *
     * @see https://github.com/orchestral/testbench#custom-service-provider
     *
     * @param  Application  $app
     *
     * @return array
     */
    protected function getPackageProviders($app): array
    {
        return [
            OpengraphLaravelServiceProvider::class,
        ];
    }
}
