<?php

namespace OpengraphLaravel\OpengraphLaravel\Tests;

enum TestEnum: string
{
    case One = 'one';
    case Two = 'two';
}
