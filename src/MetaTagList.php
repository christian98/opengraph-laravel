<?php

namespace OpengraphLaravel\OpengraphLaravel;

use BackedEnum;
use DateTime;
use Illuminate\Support\Collection;

class MetaTagList
{
    /**
     * @var Collection<int, OpenGraphMetaTag>
     */
    protected readonly Collection $metaTags;

    public function __construct()
    {
        $this->metaTags = new Collection();
    }

    /**
     * @param string $key
     * @param string|bool|DateTime|BackedEnum|float|int|null|string[]|bool[]|DateTime[]|BackedEnum[]|float[]|int[] $content
     * @return static
     */
    public function add(string $key, array|string|bool|DateTime|BackedEnum|float|int|null $content = null): static
    {
        if (!is_null($content) && !is_array($content)) {
            $this->metaTags->push(new OpenGraphMetaTag($key, $content));
        } elseif (!is_null($content)) {
            $this->metaTags->push(
                ...collect($content)
                ->map(function (string|bool|DateTime|BackedEnum|float|int $value) use ($key) {
                    return new OpenGraphMetaTag($key, $value);
                })
            )->all();
        }

        return $this;
    }

    /**
     * @param MetaTagList|PropertyCollection|null $metaTagList
     * @return static
     */
    public function addFrom(MetaTagList|PropertyCollection|null $metaTagList): static
    {
        if (is_null($metaTagList)) {
            return $this;
        }

        if ($metaTagList instanceof PropertyCollection) {
            return $this->addFrom($metaTagList->toMetaTags());
        }

        return $this->addAll(...$metaTagList->all());
    }

    /**
     * @param MetaTagList ...$metaTagList
     * @return static
     */
    public function addFromAll(MetaTagList|PropertyCollection ...$metaTagList): static
    {
        collect($metaTagList)
            ->each(function (MetaTagList|PropertyCollection $list) {
                $this->addFrom($list);
            });

        return $this;
    }

    /**
     * @param OpenGraphMetaTag ...$metaTags
     * @return static
     */
    public function addAll(OpenGraphMetaTag ...$metaTags): static
    {
        $this->metaTags->push(...$metaTags);

        return $this;
    }

    /**
     * @return array<OpenGraphMetaTag>
     */
    public function all(): array
    {
        return $this->metaTags->values()->all();
    }

    /**
     * @return Collection<int, OpenGraphMetaTag>
     */
    public function collect(): Collection
    {
        return $this->metaTags->collect();
    }
}
