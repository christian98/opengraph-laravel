<?php

namespace OpengraphLaravel\OpengraphLaravel\ObjectType;

use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class Book extends ObjectType
{
    /**
     * @return string
     */
    protected function prefix(): string
    {
        return 'book';
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return (new MetaTagList())
            ->add('og:type', 'book');
    }
}
