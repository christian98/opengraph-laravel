<?php

namespace OpengraphLaravel\OpengraphLaravel\ObjectType\Music;

use InvalidArgumentException;
use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class Song extends Music
{
    protected ?int $duration = null;

    protected ?string $album = null;

    protected ?int $albumDisk = null;

    protected ?int $albumTrack = null;

    protected ?string $musician = null;

    /**
     * @param int $seconds
     * @return static
     */
    public function duration(int $seconds): static
    {
        if ($seconds < 1) {
            throw new InvalidArgumentException('The duration must be greater or equal than 1');
        }

        $this->duration = $seconds;

        return $this;
    }

    /**
     * @param string $albumUrl
     * @return static
     */
    public function album(string $albumUrl): static
    {
        $this->album = $albumUrl;

        return $this;
    }

    /**
     * @param int $diskIndex
     * @return static
     */
    public function albumDisk(int $diskIndex): static
    {
        if ($diskIndex < 1) {
            throw new InvalidArgumentException('The disk index must be greater or equal than 1');
        }

        $this->albumDisk = $diskIndex;

        return $this;
    }

    /**
     * @param int $trackIndex
     * @return static
     */
    public function albumTrack(int $trackIndex): static
    {
        if ($trackIndex < 1) {
            throw new InvalidArgumentException('The track index must be greater or equal than 1');
        }

        $this->albumTrack = $trackIndex;

        return $this;
    }

    /**
     * @param string $musicianUrl
     * @return static
     */
    public function musician(string $musicianUrl): static
    {
        $this->musician = $musicianUrl;

        return $this;
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return (new MetaTagList())
            ->add('og:type', 'music.song')
            ->add($this->buildKey('duration'), $this->duration)
            ->add($this->buildKey('album'), $this->album)
            ->add($this->buildKey('album:disk'), $this->albumDisk)
            ->add($this->buildKey('album:track'), $this->albumTrack)
            ->add($this->buildKey('musician'), $this->musician);
    }
}
