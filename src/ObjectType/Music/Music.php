<?php

namespace OpengraphLaravel\OpengraphLaravel\ObjectType\Music;

use OpengraphLaravel\OpengraphLaravel\ObjectType\ObjectType;

abstract class Music extends ObjectType
{
    protected function prefix(): string
    {
        return 'music';
    }
}
