<?php

namespace OpengraphLaravel\OpengraphLaravel\ObjectType;

use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class Profile extends ObjectType
{
    protected ?string $firstName = null;

    protected ?string $lastName = null;

    protected ?string $username = null;

    protected ?ProfileGender $gender = null;

    /**
     * A name normally given to an individual by a parent or self-chosen.
     * @param string $firstName A name normally given to an individual by a parent or self-chosen.
     * @return static
     */
    public function firstName(string $firstName): static
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * A name inherited from a family or marriage and by which the individual is commonly known.
     * @param string $lastName A name inherited from a family or marriage and by which the individual is commonly known.
     * @return static
     */
    public function lastName(string $lastName): static
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * A short unique string to identify them.
     * @param string $username A short unique string to identify them.
     * @return static
     */
    public function username(string $username): static
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Their gender.
     * @param ProfileGender $gender Their gender.
     * @return static
     */
    public function gender(ProfileGender $gender): static
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * @return string
     */
    protected function prefix(): string
    {
        return 'profile';
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return (new MetaTagList())
            ->add('og:type', 'profile')
            ->add($this->buildKey('first_name'), $this->firstName)
            ->add($this->buildKey('last_name'), $this->lastName)
            ->add($this->buildKey('username'), $this->username)
            ->add($this->buildKey('gender'), $this->gender);
    }
}
