<?php

namespace OpengraphLaravel\OpengraphLaravel\ObjectType;

enum ProfileGender: string
{
    case Male = 'male';
    case Female = 'female';
}
