<?php

namespace OpengraphLaravel\OpengraphLaravel\ObjectType;

use DateTime;
use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class Article extends ObjectType
{
    protected string|DateTime|null $publishedTime = null;

    protected string|DateTime|null $modifiedTime = null;

    protected string|DateTime|null $expirationTime = null;

    protected ?string $author = null;

    protected ?string $section = null;

    /**
     * @var string[]
     */
    protected array $tags = [];

    /**
     * @param string|DateTime $publishedTime
     * @return static
     */
    public function publishedTime(string|DateTime $publishedTime): static
    {
        $this->publishedTime = $publishedTime;

        return $this;
    }

    /**
     * @param string|DateTime $modifiedTime
     * @return static
     */
    public function modifiedTime(string|DateTime $modifiedTime): static
    {
        $this->modifiedTime = $modifiedTime;

        return $this;
    }

    /**
     * @param string|DateTime $expirationTime
     * @return static
     */
    public function expirationTime(string|DateTime $expirationTime): static
    {
        $this->expirationTime = $expirationTime;

        return $this;
    }

    /**
     * @param string $authorUrl
     * @return static
     */
    public function author(string $authorUrl): static
    {
        $this->author = $authorUrl;

        return $this;
    }

    /**
     * @param string $section
     * @return static
     */
    public function section(string $section): static
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @param string $tag
     * @return static
     */
    public function tag(string $tag): static
    {
        return $this->tags([$tag]);
    }

    /**
     * @param array<string> $tags
     * @return static
     */
    public function tags(array $tags): static
    {
        $this->tags = collect($this->tags)
            ->push(...$tags)
            ->unique()
            ->values()
            ->all();

        return $this;
    }

    /**
     * @return string
     */
    protected function prefix(): string
    {
        return 'article';
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        $list = (new MetaTagList())
            ->add('og:type', 'article')
            ->add($this->buildKey('published_time'), $this->publishedTime)
            ->add($this->buildKey('modified_time'), $this->modifiedTime)
            ->add($this->buildKey('expiration_time'), $this->expirationTime)
            ->add($this->buildKey('author'), $this->author);

        foreach ($this->tags as $tag) {
            $list->add($this->buildKey('tag'), $tag);
        }

        return $list;
    }
}
