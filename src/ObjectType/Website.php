<?php

namespace OpengraphLaravel\OpengraphLaravel\ObjectType;

use LogicException;
use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class Website extends ObjectType
{
    protected function prefix(): string
    {
        throw new LogicException('nope');
    }

    /**
     * @inheritDoc
     */
    public function toMetaTags(): MetaTagList
    {
        return (new MetaTagList())
            ->add('og:type', 'website');
    }
}
