<?php

namespace OpengraphLaravel\OpengraphLaravel\Basic;

use JetBrains\PhpStorm\Pure;
use OpengraphLaravel\OpengraphLaravel\MetaTagList;
use OpengraphLaravel\OpengraphLaravel\OpenGraphData;
use OpengraphLaravel\OpengraphLaravel\PropertyCollection;

abstract class Media extends PropertyCollection
{
    private ?string $secureUrl = null;

    private ?string $mimeType = null;

    /**
     * @param string $fileUrl
     * @param OpenGraphData $openGraphData
     */
    #[Pure]
    public function __construct(public readonly string $fileUrl, protected readonly OpenGraphData $openGraphData)
    {
    }

    /**
     * @param string $fileUrl
     * @return static
     */
    public function secureUrl(string $fileUrl): static
    {
        $this->secureUrl = $fileUrl;

        return $this;
    }

    /**
     * @param string $mimeType
     * @return static
     */
    public function mimeType(string $mimeType): static
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return (new MetaTagList())
            ->add($this->prefix(), $this->fileUrl)
            ->add($this->buildKey('secure_url'), $this->secureUrl)
            ->add($this->buildKey('type'), $this->mimeType);
    }
}
