<?php

namespace OpengraphLaravel\OpengraphLaravel\Basic;

class Image extends PixelMedia
{
    protected function prefix(): string
    {
        return $this->openGraphData->prefix().':image';
    }
}
