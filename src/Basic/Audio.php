<?php

namespace OpengraphLaravel\OpengraphLaravel\Basic;

class Audio extends Media
{
    protected function prefix(): string
    {
        return $this->openGraphData->prefix() . ':audio';
    }
}
