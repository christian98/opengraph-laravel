<?php

namespace OpengraphLaravel\OpengraphLaravel\Basic;

class Video extends PixelMedia
{
    protected function prefix(): string
    {
        return $this->openGraphData->prefix() . ':video';
    }
}
