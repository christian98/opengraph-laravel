<?php

namespace OpengraphLaravel\OpengraphLaravel\Basic;

use OpengraphLaravel\OpengraphLaravel\MetaTagList;

abstract class PixelMedia extends Media
{
    protected ?string $width = null;

    protected ?string $height = null;

    protected ?string $alt = null;

    /**
     * @param string $pixelWidth
     * @return static
     */
    public function width(string $pixelWidth): static
    {
        $this->width = $pixelWidth;

        return $this;
    }

    /**
     * @param string $pixelHeight
     * @return static
     */
    public function height(string $pixelHeight): static
    {
        $this->height = $pixelHeight;

        return $this;
    }

    /**
     * @param string $description
     * @return static
     */
    public function alt(string $description): static
    {
        $this->alt = $description;

        return $this;
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return (new MetaTagList())
            ->addFrom(parent::toMetaTags())
            ->add($this->buildKey('width'), $this->width)
            ->add($this->buildKey('height'), $this->height)
            ->add($this->buildKey('alt'), $this->alt);
    }
}
