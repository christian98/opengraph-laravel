<?php

namespace OpengraphLaravel\OpengraphLaravel\Twitter;

enum AppStoreType: string
{
    case IPhone = 'iphone';
    case IPad = 'ipad';
    case GooglePlay = 'googleplay';
}
