<?php

namespace OpengraphLaravel\OpengraphLaravel\Twitter;

use Illuminate\Support\Str;
use OpengraphLaravel\OpengraphLaravel\MetaTagList;
use OpengraphLaravel\OpengraphLaravel\PropertyCollection;

abstract class TwitterCard extends PropertyCollection
{
    protected ?string $site = null;

    /**
     * @param string $siteHandle
     * @return static
     */
    public function site(string $siteHandle): static
    {
        $this->site = Str::start($siteHandle, '@');

        return $this;
    }

    /**
     * @inheritDoc
     */
    protected function prefix(): string
    {
        return 'twitter';
    }

    /**
     * @inheritDoc
     */
    public function toMetaTags(): MetaTagList
    {
        // TODO better use name instead of property for the key
        return (new MetaTagList())
            ->add($this->buildKey('card'), $this->cardType()->value)
            ->add($this->buildKey('site'), $this->site);
    }

    /**
     * @return TwitterCardType
     */
    abstract protected function cardType(): TwitterCardType;
}
