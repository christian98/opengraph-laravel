<?php

namespace OpengraphLaravel\OpengraphLaravel\Twitter;

enum TwitterCardType: string
{
    case Summary = 'summary';
    case SummaryLargeImage = 'summary_large_image';
    case App = 'app';
    case Player = 'player';
}
