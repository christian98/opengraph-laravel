<?php

namespace OpengraphLaravel\OpengraphLaravel\Twitter;

use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class PlayerTwitterCard extends TwitterCard
{
    protected ?string $title = null;

    protected ?string $description = null;

    protected ?string $image = null;

    protected ?string $imageAlt = null;

    protected ?string $playerUrl = null;

    protected ?string $playerWidth = null;

    protected ?string $playerHeight = null;

    /**
     * @param string $title
     * @return static
     */
    public function title(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $description
     * @return static
     */
    public function description(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param string $image
     * @return static
     */
    public function image(string $image): static
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param string $imageAlt
     * @return static
     */
    public function imageAlt(string $imageAlt): static
    {
        $this->imageAlt = $imageAlt;

        return $this;
    }

    /**
     * @param string $playerUrl
     * @return static
     */
    public function playerUrl(string $playerUrl): static
    {
        $this->playerUrl = $playerUrl;

        return $this;
    }

    /**
     * @param string $playerWidth
     * @return static
     */
    public function playerWidth(string $playerWidth): static
    {
        $this->playerWidth = $playerWidth;

        return $this;
    }

    /**
     * @param string $playerHeight
     * @return static
     */
    public function playerHeight(string $playerHeight): static
    {
        $this->imageAlt = $playerHeight;

        return $this;
    }

    /**
     * @return TwitterCardType
     */
    protected function cardType(): TwitterCardType
    {
        return TwitterCardType::Player;
    }

    public function toMetaTags(): MetaTagList
    {
        return parent::toMetaTags()
            ->add($this->buildKey('title'), $this->title)
            ->add($this->buildKey('description'), $this->description)
            ->add($this->buildKey('image'), $this->image)
            ->add($this->buildKey('image:alt'), $this->imageAlt)
            ->add($this->buildKey('player'), $this->playerUrl)
            ->add($this->buildKey('player:width'), $this->playerWidth)
            ->add($this->buildKey('player:height'), $this->playerHeight);
    }
}
