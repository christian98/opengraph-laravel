<?php

namespace OpengraphLaravel\OpengraphLaravel\Twitter;

use Illuminate\Support\Str;
use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class SummaryTwitterCard extends TwitterCard
{
    protected ?string $creator = null;

    protected ?string $title = null;

    protected ?string $description = null;

    protected ?string $image = null;

    protected ?string $imageAlt = null;

    protected TwitterCardType $cardType = TwitterCardType::Summary;

    /**
     * @param string $creatorHandle
     * @return static
     */
    public function creator(string $creatorHandle): static
    {
        $this->creator = Str::start($creatorHandle, '@');

        return $this;
    }

    /**
     * @param string $title
     * @return static
     */
    public function title(string $title): static
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @param string $description
     * @return static
     */
    public function description(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param string $image
     * @return static
     */
    public function image(string $image): static
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @param string $imageAlt
     * @return static
     */
    public function imageAlt(string $imageAlt): static
    {
        $this->imageAlt = $imageAlt;

        return $this;
    }

    /**
     * @param bool $largeImage
     * @return static
     */
    public function largeImage(bool $largeImage = true): static
    {
        $this->cardType = $largeImage ? TwitterCardType::SummaryLargeImage : TwitterCardType::Summary;

        return $this;
    }

    /**
     * @return TwitterCardType
     */
    protected function cardType(): TwitterCardType
    {
        return $this->cardType;
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return parent::toMetaTags()
            ->add($this->buildKey('creator'), $this->creator)
            ->add($this->buildKey('title'), $this->title)
            ->add($this->buildKey('description'), $this->description)
            ->add($this->buildKey('image'), $this->image)
            ->add($this->buildKey('image:alt'), $this->imageAlt);
    }
}
