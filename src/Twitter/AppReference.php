<?php

namespace OpengraphLaravel\OpengraphLaravel\Twitter;

use Illuminate\Support\Str;
use OpengraphLaravel\OpengraphLaravel\MetaTagList;
use OpengraphLaravel\OpengraphLaravel\ObjectType\ObjectType;

class AppReference extends ObjectType
{
    protected ?string $name = null;

    protected ?string $id = null;

    protected ?string $url = null;

    /**
     * @param AppStoreType $appStoreType
     */
    public function __construct(
        protected readonly AppStoreType $appStoreType,
    ) {
    }

    /**
     * @param string $name
     * @return static
     */
    public function name(string $name): static
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param string $id
     * @return static
     */
    public function id(string $id): static
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param string $url
     * @return static
     */
    public function url(string $url): static
    {
        $this->url = $url;

        return $this;
    }

    /**
     * @return string
     */
    protected function prefix(): string
    {
        return 'twitter:app';
    }

    /**
     * @param string $name
     * @return string
     */
    protected function buildKey(string $name): string
    {
        return Str::finish(parent::buildKey($name), ':') . $this->appStoreType->value;
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return (new MetaTagList())
            ->add($this->buildKey('name'), $this->name)
            ->add($this->buildKey('id'), $this->id)
            ->add($this->buildKey('url'), $this->url);
    }
}
