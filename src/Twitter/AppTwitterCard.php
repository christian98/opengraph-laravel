<?php

namespace OpengraphLaravel\OpengraphLaravel\Twitter;

use OpengraphLaravel\OpengraphLaravel\MetaTagList;

class AppTwitterCard extends TwitterCard
{
    protected ?string $description = null;

    protected ?AppReference $iPhone = null;

    protected ?AppReference $iPad = null;

    protected ?AppReference $googlePlay = null;

    protected ?string $country = null;

    /**
     * @param string $description
     * @return static
     */
    public function description(string $description): static
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @param callable(AppReference): void $alter
     * @return static
     */
    public function iPhone(callable $alter): static
    {
        $this->iPhone = new AppReference(AppStoreType::IPhone);

        $alter($this->iPhone);

        return $this;
    }

    /**
     * @param callable(AppReference): void $alter
     * @return static
     */
    public function iPad(callable $alter): static
    {
        $this->iPad = new AppReference(AppStoreType::IPad);

        $alter($this->iPad);

        return $this;
    }

    /**
     * @param callable(AppReference): void $alter
     * @return static
     */
    public function googlePlay(callable $alter): static
    {
        $this->googlePlay = new AppReference(AppStoreType::GooglePlay);

        $alter($this->googlePlay);

        return $this;
    }

    /**
     * @param string $country
     * @return static
     */
    public function country(string $country): static
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return TwitterCardType
     */
    protected function cardType(): TwitterCardType
    {
        return TwitterCardType::App;
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        return parent::toMetaTags()
            ->add($this->buildKey('description'), $this->description)
            ->addFrom($this->iPhone)
            ->addFrom($this->iPad)
            ->addFrom($this->googlePlay)
            ->add($this->buildKey('country'), $this->country);
    }
}
