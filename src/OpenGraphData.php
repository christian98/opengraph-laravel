<?php

namespace OpengraphLaravel\OpengraphLaravel;

use Illuminate\Contracts\Support\Htmlable;
use InvalidArgumentException;
use LogicException;
use OpengraphLaravel\OpengraphLaravel\Basic\Audio;
use OpengraphLaravel\OpengraphLaravel\Basic\Image;
use OpengraphLaravel\OpengraphLaravel\Basic\Video;
use OpengraphLaravel\OpengraphLaravel\ObjectType\Article;
use OpengraphLaravel\OpengraphLaravel\ObjectType\Book;
use OpengraphLaravel\OpengraphLaravel\ObjectType\Music\Song;
use OpengraphLaravel\OpengraphLaravel\ObjectType\ObjectType;
use OpengraphLaravel\OpengraphLaravel\ObjectType\Profile;
use OpengraphLaravel\OpengraphLaravel\ObjectType\Website;
use OpengraphLaravel\OpengraphLaravel\Twitter\AppTwitterCard;
use OpengraphLaravel\OpengraphLaravel\Twitter\PlayerTwitterCard;
use OpengraphLaravel\OpengraphLaravel\Twitter\SummaryTwitterCard;
use OpengraphLaravel\OpengraphLaravel\Twitter\TwitterCard;

class OpenGraphData extends PropertyCollection implements Htmlable
{
    private bool $show = false;

    protected ?string $title = null;

    protected ?string $description = null;

    protected ?string $determiner = null;

    protected ?string $locale = null;

    /**
     * @var string[]
     */
    protected array $localeAlternate = [];

    protected ?string $siteName = null;

    protected ?string $url = null;

    protected ?ObjectType $objectType = null;

    /**
     * @var Image[]
     */
    protected array $images = [];

    /**
     * @var Video[]
     */
    protected array $videos = [];

    /**
     * @var Audio[]
     */
    protected array $audios = [];

    /**
     * @var TwitterCard|null
     */
    protected ?TwitterCard $twitter = null;

    /**
     * @param string $title
     * @return static
     */
    public function title(string $title): static
    {
        $this->title = $title;

        return $this->show();
    }

    /**
     * @param string $description
     * @return static
     */
    public function description(string $description): static
    {
        $this->description = $description;

        return $this->show();
    }

    /**
     * @param string $determiner
     * @return static
     */
    public function determiner(string $determiner): static
    {
        $this->determiner = $determiner;

        return $this->show();
    }

    /**
     * @param string $locale
     * @return static
     */
    public function locale(string $locale): static
    {
        $this->locale = $locale;

        return $this->show();
    }

    /**
     * @param string $locale
     * @return static
     */
    public function localeAlternate(string $locale): static
    {
        $this->localeAlternate = collect($this->localeAlternate)
            ->push($locale)
            ->unique()
            ->values()
            ->all();

        return $this->show();
    }

    /**
     * @param string $siteName
     * @return static
     */
    public function siteName(string $siteName): static
    {
        $this->siteName = $siteName;

        return $this->show();
    }

    /**
     * @param string   $filename
     * @param callable $options
     * @return static
     */
    public function image(string $filename, ?callable $options = null): static
    {
        $image = new Image($filename, $this);

        if (!is_null($options)) {
            $options($image);
        }

        $this->images = collect($this->images)
            ->filter(function (Image $collectedImage) use ($image) {
                return $collectedImage->fileUrl !== $image->fileUrl;
            })
            ->push($image)
            ->all();

        return $this->show();
    }

    /**
     * @return static
     */
    public function clearImages(): static
    {
        $this->images = [];

        return $this;
    }

    /**
     * @param string   $filename
     * @param callable $options
     * @return static
     */
    public function video(string $filename, ?callable $options = null): static
    {
        $video = new Video($filename, $this);

        if (!is_null($options)) {
            $options($video);
        }

        $this->videos = collect($this->videos)
            ->filter(function (Video $collectedVideo) use ($video) {
                return $collectedVideo->fileUrl !== $video->fileUrl;
            })
            ->push($video)
            ->all();

        return $this->show();
    }

    /**
     * @return static
     */
    public function clearVideos(): static
    {
        $this->videos = [];

        return $this;
    }

    /**
     * @param string   $filename
     * @param callable $options
     * @return static
     */
    public function audio(string $filename, ?callable $options = null): static
    {
        $audio = new Audio($filename, $this);

        if (!is_null($options)) {
            $options($audio);
        }

        $this->audios = collect($this->audios)
            ->filter(function (Audio $collectedAudio) use ($audio) {
                return $collectedAudio->fileUrl !== $audio->fileUrl;
            })
            ->push($audio)
            ->all();

        return $this->show();
    }

    /**
     * @return static
     */
    public function clearAudios(): static
    {
        $this->audios = [];

        return $this;
    }

    /**
     * @param string $url
     * @return static
     */
    public function url(string $url): static
    {
        $this->url = $url;

        return $this->show();
    }

    /**
     * @param callable(Song): void $alter
     * @return static
     */
    public function typeMusicSong(callable $alter): static
    {
        $this->objectType = new Song();

        $alter($this->objectType);

        return $this->show();
    }

    /**
     * @return static
     */
    public function typeMusicAlbum(): static
    {
        throw new LogicException('This Method is not implemented yet.'); // TODO
    }

    /**
     * @return static
     */
    public function typeMusicPlaylist(): static
    {
        throw new LogicException('This Method is not implemented yet.'); // TODO
    }

    /**
     * @return static
     */
    public function typeMusicRadioStation(): static
    {
        throw new LogicException('This Method is not implemented yet.'); // TODO
    }

    /**
     * @return static
     */
    public function typeVideoMovie(): static
    {
        throw new LogicException('This Method is not implemented yet.'); // TODO
    }

    /**
     * @return static
     */
    public function typeVideoEpisode(): static
    {
        throw new LogicException('This Method is not implemented yet.'); // TODO
    }

    /**
     * @return static
     */
    public function typeVideoTvShow(): static
    {
        throw new LogicException('This Method is not implemented yet.'); // TODO
    }

    /**
     * @return static
     */
    public function typeVideoOther(): static
    {
        throw new LogicException('This Method is not implemented yet.'); // TODO
    }

    /**
     * @param callable $alter
     * @return static
     */
    public function typeArticle(callable $alter): static
    {
        $this->objectType = new Article();

        $alter($this->objectType);

        return $this->show();
    }

    /**
     * @param callable(Book): void $alter
     * @return static
     */
    public function typeBook(callable $alter): static
    {
        $this->objectType = new Book();

        $alter($this->objectType);

        return $this->show();
    }

    /**
     * @param callable(Profile):void $alter
     * @return static
     */
    public function typeProfile(callable $alter): static
    {
        $this->objectType = new Profile();

        $alter($this->objectType);

        return $this->show();
    }

    /**
     * @return static
     */
    public function typeWebsite(): static
    {
        $this->objectType = new Website();

        return $this->show();
    }

    /**
     * @param callable(SummaryTwitterCard): void $alter
     * @return static
     */
    public function twitterSummary(callable $alter): static
    {
        $this->twitter = new SummaryTwitterCard();

        $alter($this->twitter);

        return $this->show();
    }

    /**
     * @param callable(PlayerTwitterCard): void $alter
     * @return static
     */
    public function twitterPlayer(callable $alter): static
    {
        $this->twitter = new PlayerTwitterCard();

        $alter($this->twitter);

        return $this->show();
    }

    /**
     * @param callable(AppTwitterCard): void $alter
     * @return static
     */
    public function twitterApp(callable $alter): static
    {
        $this->twitter = new AppTwitterCard();

        $alter($this->twitter);

        return $this->show();
    }

    /**
     * @param bool $show
     * @return static
     */
    public function show(bool $show = true): static
    {
        $this->show = true;

        return $this;
    }

    /**
     * @param bool $hide
     * @return static
     */
    public function hide(bool $hide = true): static
    {
        return $this->show(!$hide);
    }

    /**
     * @return string
     */
    protected function prefix(): string
    {
        return 'og';
    }

    /**
     * @return MetaTagList
     */
    public function toMetaTags(): MetaTagList
    {
        if (!$this->show) {
            // just return an empty list...
            return new MetaTagList();
        }

        if (is_null($this->title)) {
            throw new InvalidArgumentException('The title is required for each page!');
        }

        if (is_null($this->objectType)) {
            throw new InvalidArgumentException('You need to specify an object type for each page!');
        }

        if (count($this->images) < 1) {
            throw new InvalidArgumentException('We need at least one image per page!');
        }

        if (is_null($this->url)) {
            throw new InvalidArgumentException('The url is required for each page!');
        }

        return (new MetaTagList())
            ->add($this->buildKey('title'), $this->title)
            ->add($this->buildKey('description'), $this->description)
            ->add($this->buildKey('determiner'), $this->determiner)
            ->add($this->buildKey('locale'), $this->locale)
            ->add($this->buildKey('locale:alternate'), $this->localeAlternate)
            ->add($this->buildKey('site_name'), $this->siteName)
            ->add($this->buildKey('url'), $this->url)
            ->addFromAll(...$this->images)
            ->addFromAll(...$this->videos)
            ->addFromAll(...$this->audios)
            ->addFrom($this->objectType->toMetaTags())
            ->addFrom($this->twitter);
    }

    /**
     * @inheritDoc
     */
    public function toHtml(): string
    {
        return $this->toMetaTags()
            ->collect()
            ->map(function (OpenGraphMetaTag $tag) {
                return $tag->toHtml();
            })
            ->join("\n");
    }
}
