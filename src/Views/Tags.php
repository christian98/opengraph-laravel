<?php

namespace OpengraphLaravel\OpengraphLaravel\Views;

use Illuminate\View\Component;
use OpengraphLaravel\OpengraphLaravel\OpenGraphData;

class Tags extends Component
{
    /**
     * @param OpenGraphData $openGraphData
     */
    public function __construct(protected readonly OpenGraphData $openGraphData)
    {
    }

    /**
     * @return string
     */
    public function render(): string
    {
        return $this->openGraphData->toHtml() . "\n";
    }
}
