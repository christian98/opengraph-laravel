<?php

namespace OpengraphLaravel\OpengraphLaravel;

use Illuminate\Support\Str;

abstract class PropertyCollection
{
    /**
     * @param string $name
     * @return string
     */
    protected function buildKey(string $name): string
    {
        $normalizedPrefix = Str::of($this->prefix())->trim(':');
        $normalizedSuffix = Str::of($name)->trim(':');

        return $normalizedPrefix->append(':', $normalizedSuffix)->toString();
    }

    /**
     * @return string
     */
    abstract protected function prefix(): string;

    /**
     * @return MetaTagList
     */
    abstract public function toMetaTags(): MetaTagList;
}
