<?php

namespace OpengraphLaravel\OpengraphLaravel;

use Illuminate\Support\Facades\Facade;
use JetBrains\PhpStorm\Pure;

class Opengraph extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return OpenGraphData::class;
    }

    /**
     * @return OpenGraphData
     */
    #[Pure] public static function builder(): OpenGraphData
    {
        return new OpenGraphData();
    }

    /**
     * @param OpenGraphData $openGraphData
     * @return void
     */
    public static function use(OpenGraphData $openGraphData): void
    {
        static::swap($openGraphData);
    }
}
