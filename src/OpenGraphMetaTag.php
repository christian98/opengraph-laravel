<?php

namespace OpengraphLaravel\OpengraphLaravel;

use BackedEnum;
use DateTime;
use Illuminate\Contracts\Support\Htmlable;

class OpenGraphMetaTag implements Htmlable
{
    /**
     * @param  string  $property
     * @param  string|bool|DateTime|BackedEnum|float|int  $content
     */
    public function __construct(
        public readonly string $property,
        public readonly string|bool|DateTime|BackedEnum|float|int $content,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function toHtml(): string
    {
        $property = e($this->property);
        $content = e($this->formattedContent());

        return "<meta property=\"$property\" content=\"$content\" />";
    }

    /**
     * @return string
     */
    protected function formattedContent(): string
    {
        if (is_bool($this->content)) {
            return $this->content ? 'true' : 'false';
        }

        if ($this->content instanceof DateTime) {
            return $this->content->format('c');
        }

        if ($this->content instanceof BackedEnum) {
            return (string) $this->content->value;
        }

        // used as a fallback
        return (string) $this->content;
    }
}
