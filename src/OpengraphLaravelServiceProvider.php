<?php

namespace OpengraphLaravel\OpengraphLaravel;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;

class OpengraphLaravelServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../config/opengraph.php', 'opengraph');

        $this->app->scoped(OpenGraphData::class, fn () => new OpenGraphData());
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        $this->publishes([
            __DIR__.'/../config/opengraph.php' => config_path('opengraph.php'),
        ]);

        Blade::componentNamespace('OpengraphLaravel\\OpengraphLaravel\\Views', 'opengraph');
    }
}
